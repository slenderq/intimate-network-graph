{
  description = "intimate-network-graph";

  # TODO: remove comments once we know if we need a poetry.

  inputs = {
    nixpkgs = { url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication mkPoetryEnv defaultPoetryOverrides;
        python = pkgs.python311Full;
        pythonPkgs = pkgs.python311Packages;


        pypkgs-build-requirements = {
          # ollama = [ "poetry" ];
        };
        poetryOverrides = defaultPoetryOverrides.extend (self: super:
          builtins.mapAttrs
            (package: build-requirements:
              (builtins.getAttr package super).overridePythonAttrs (old: {
                buildInputs = (old.buildInputs or [ ]) ++ (builtins.map (pkg: if builtins.isString pkg then builtins.getAttr pkg super else pkg) build-requirements);
              })
            )
            pypkgs-build-requirements
        );
        appEnv = mkPoetryEnv {
          projectDir = self;
          editablePackageSources = {
            graph = ./src;
          };
          overrides = poetryOverrides;
        };
        appPkgs = with pkgs; [
          poetry
          nixpkgs-fmt
          mermaid-cli
          lsix # ls for images
          graphviz
        ];

        customScripts = [
          (pkgs.writeShellApplication {
            name = "build-graph-mermaid";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              set -e
              mmdc -i graph.mmd -o graph.png -t dark -b transparent
            '';
          })
          (pkgs.writeShellApplication {
            name = "build-graph-graphviz";
            runtimeInputs = [ ];
            text = ''
              #!/usr/bin/env bash
              set -e
              python src/graph/main.py #create Source.gv
              twopi -Tpng -Gscale=1 -GK=10 -Goverlap=voronoi -Gmaxiter=3000 -Gsplines=curved  Source.gv -o twopi.png
              sfdp -Tpng -Gscale=1 -GK=0.8 -Goverlap=voronoi -Gmaxiter=3000 -Gsplines=curved  Source.gv -o sfdp-low.png
              sfdp -Tpng -Gscale=1 -GK=3 -Goverlap=voronoi -Gmaxiter=3000 -Gsplines=curved  Source.gv -o sfdp-high.png
              # sfdp -Tpng -Gscale=1 -Goverlap=false Source.gv -o fdp.png
            '';
          })
        ];
      in
      rec {
        packages = {
          # https://ryantm.github.io/nixpkgs/languages-frameworks/python/#buildpythonpackage-function
          # https://github.com/nix-community/poetry2nix/blob/master/docs/edgecases.md
          graph = mkPoetryApplication {
            projectDir = self;
            overrides = poetryOverrides;
          };
          default = self.packages.${system}.graph;
        };

        # defaultPackage = packages.friendly-robots;
        devShells = {
          poetry = pkgs.mkShell {
            inputsFrom = [ self.packages.${system}.graph ];
            nativeBuildInputs = appPkgs ++ customScripts ++ [ appEnv ];
            # nativeBuildInputs = appPkgs ++ customScripts;
          };
        };

        devShell = devShells.poetry;

      });
}




