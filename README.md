# intimate-network-graph

![graph](./sfdp-low.png)


## Other layouts
![graph](./sfdp-high.png)
![graph](./twopi.png)
![graph](./Source.gv.png)

## Setup

### rebuild the graph

```
nix develop
build-graph
```

## Links
- https://github.com/mermaid-js/mermaid
- https://graphs.grevian.org/example
- https://mermaid.live/edit#pako:eNpdkEFvgzAMhf8K8nEqpYSNthx22Xbcqcexg0sCiZQQlDhIFeK_L8A6TfXp6fOz9ewJGssFVOAJSbwr7ByadGR1n8T6evpO0vQ1uZDSekOrXGFsPqJPO6q-2-imH8f_0TeHXm50lfelsAMjnEHFY6xpMdRAUhhRQxUlFy0GTTXU_RytYeAx-AdXZB1ULWovdoCB7OXWN1CRC-Ju-r3uz6UtchGHJqDbsPygU57iysb2reoWHpyOWBINvsqypb3vFMlw3TfWZF5xiY7keC6zkpUnZIUojwW-FAVvrvn51LLnvOXHQ84Q5nn-AVtLcwk


