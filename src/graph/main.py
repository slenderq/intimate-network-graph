import graphviz

# sep="1"
g = graphviz.Source('''
graph G {
    node [style=filled , shape=egg, colorscheme=dark28]
    sep="1.1"
    Justin [color=1];
    Jasmine [color=2];
    Levon [color=3];
    Mer [color=4];
    Skyler [color=5];
    Al [color=6];
    Gill [color=7];
    Grant [color=8];
    Mer -- Skyler [weight=200];
    Mer -- Al [weight=200];
    Mer -- Grant [weight=200, style=dashed];
    Skyler -- Al [weight=200];
    Justin -- Al [weight=125, style=dashed];
    Justin -- Grant [weight=10, style=dotted];
    Justin -- Skyler [weight=125, style=dashed];
    Justin -- Gill [weight=10, style=dotted];
    Grant -- Gill [weight=200];
    Jasmine -- Justin [weight=200, style=dashed];
    Justin -- Mer [weight=200];
    Gill -- Al [weight=10, style=dotted];
    Gill -- Skyler [weight=10, style=dotted];
    Grant -- Al [weight=10, style=dotted];
    Grant -- Skyler [weight=10, style=dotted];
    Jasmine -- Al [weight=125, style=dashed];
    Jasmine -- Mer [weight=125, style=dashed];
    Jasmine -- Skyler [weight=125, style=dashed];
    Levon -- Justin [weight=10, style=dotted];
}
''',
    engine="dot", format="png")

g.render()
